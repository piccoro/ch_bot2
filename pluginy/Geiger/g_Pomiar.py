from silf_errbot_base.exp_base import *
import json


class g_Pomiar(pomiar):

    u_start = 0
    u_stop = 0

    opis_etap = [[0, "Poczatek pomiaru, zobaczymy cos dopiero przy napieciu 400 [V]", 10],
         [400, "Tu zaczyna sie prawdziwy pomiar! Otrzymamy punkty na wzglednie poddobnym poziomie", 403],
         [550, "Tu zaczyna sie skok wartosci :)", 615],
         [900, "Koniec pomiaru jest bliski...)", 2300]]

    opis_serie = ["Dobrze że pytasz! Mogę teraz wykonać tylko jedną serię pomiarową!",
                  "Seria 1: Skan ogólny całego przedziału napięć. By ją wykonać napisz:",
                  "!\\czesio series start",
                  "Aby przerwac serie napisz:",
                  "!\\czesio series stop"]
    dane_ok = []
    dane_t = []
    czas_t = 0
    rozrzut = 0.01

    def __init__(self, u_start=0, u_stop=0, czas_pomiaru=0, liczba_punktow=0):
        #self.plugin_dir = holder.bot.plugin_dir
        self.u_start = u_start
        self.u_stop = u_stop
        super(g_Pomiar, self).__init__(czas_pomiaru, liczba_punktow)

    def g_Pomiar(self, u_start=0, u_stop=0, czas_pomiaru=0, liczba_punktow=0):
        self.__init__(u_start, u_stop, czas_pomiaru, liczba_punktow)

    def opis(self):
        messs = "Parametry pomiaru, Napięcie poczatkowe {}[V], napięcie końcowe {}[V], czas pomiaru punktu {}[s], ilość punktów {}".format(self.u_start, self.u_stop, self.czas_pomiaru, self.liczba_punktow)
        return messs

    def napiecie(self, mess, json_ok = False):
        if not json_ok:
            #TODO jesli jest to ok to nie trzeba ponizszego czyscenia
            m0 = str(mess)
            m1 = m0.find("silf:results")
            if m1 >= 0:
                m1 = m0.replace("silf:results","")
                mess = json.loads(m1)
        dane = mess
        try:
            nap = dane["chart"]["value"]
            nap = nap[-1][0]
        except KeyError:
            #print("wartosc 'napiecia': {}".format(nap))
            pass
        return nap

    def czas(self, mess):
        m0 = str(mess)
        m1 = m0.find("silf:series:start")
        if m1 >= 0:
            m1 = m0.replace("silf:series:start", "")
            dane = json.loads(m1)
            czas = dane["initialSettings"]["acquisition_time"]["value"]
            return czas
        return 0

    def opisexp(self, mess):

        # sprawdzanie czy wartosci sie zgadzaja
        if self.czas_t == 0:
            czas = self.czas(mess)
            if czas > 0:
                self.czas_t = czas
        if self.dane_ok == []:
            if self.etap >= 0 and self.etap <= 10:
                tt = self.pomiar(mess)
                if tt != -1:
                    self.dane_t = tt
                #else:
                    #print("")
                    #print("\n\n\n\n\n\nUwaga pommiar -1 tresc wiad:\n{}\n\ntt:\n\n{}\n\n".format(mess, tt))
        else:
            if self.czas_t == 0:
                czas = self.czas(mess)
                if czas > 0:
                    self.czas_t = czas
            c = self.pomiar(mess)
            if c is not None:
                self.dane_t = c
                cc = self.test()
                if cc is not None:
                    return cc

        # opis dla uczestnikow exp
        if self.etap == 4:
            return None
        elif self.etap == 0:
            self.etap = 1
            return "{}".format(self.opis_etap[self.etap-1][1])
        elif self.etap == 1:
            if self.napiecie(mess) >= self.opis_etap[self.etap-1][0]:
                self.etap = 2
                return "{}".format(self.opis_etap[self.etap][1])
        elif self.etap == 2:
            if self.napiecie(mess) >= self.opis_etap[self.etap][0]:
                self.etap = 3
                return "{}".format(self.opis_etap[self.etap-1][1])
        elif self.etap == 3:
            if self.napiecie(mess) >= self.opis_etap[self.etap][0]:
                self.etap = 4
                return "{}".format(self.opis_etap[self.etap-1][1])
        else:
            print("Blad w trakcie experymentu ")
        return None

    def pomiar(self, mess):
        m0 = str(mess)
        if m0.find("silf:series:stop") >= 0:
            return -1
        m1 = m0.find("silf:results")
        if m1 >= 0:
            m1 = m0.replace("silf:results", "")
            dane = json.loads(m1)
            pom = dane["chart"]["value"]
            return pom
        return None

    def test(self, ostat = False):
        c = self.dane_t
        l = len(c)
        l0 = len(self.dane_ok)
        dobre_dane = True
        if l == l0:
            print("")
        elif l > 1:
            i = 0
            while True:
                i = i + 1
                if i >= l0:
                    break
                if c[-1][0] < self.dane_ok[i][0]:
                    break
            i = i -1
            if i < 0:
                i = 0
            if ostat:
                if ((c[-1][1] < self.dane_ok[-1][1]*(1-self.rozrzut))) or (c[-1][1] > self.dane_ok[-1][1]*(1+self.rozrzut)):
                    dobre_dane = False
            else:
                if ((c[-2][1] < self.dane_ok[i-1][1]*(1-self.rozrzut))) or (c[-2][1] > self.dane_ok[i-1][1]*(1+self.rozrzut)):
                    dobre_dane = False
        if dobre_dane:
            return None
        else:
            print("cos jest nie tak z danymi pomiarowymi badz zmienily sie ustawienia pomiaru")
            return "wartosci sa nie prawidlowe wzgledem danych referencyjnych [{}] [{}]".format(c[-2][1], self.dane_ok[i-1][1])

    def zapisz_dane(self):
        if self.dane_t != []:
            self.dane_ok = self.dane_t
            if self.czas_t > 0:
                for x in self.dane_ok:
                    x[1] = x[1]/self.czas_t
