from silf_errbot_base.silf_errbot_plugin_base import *
from Geiger.g_Pomiar import *

class Geiger(silf_plugin_base):

    moj_g_pomiar = g_Pomiar()

    def __init__(self):
        super(Geiger, self).__init__(self.moj_g_pomiar, "geiger-chat@muc.debian", "geiger@muc.debian/experiment")

