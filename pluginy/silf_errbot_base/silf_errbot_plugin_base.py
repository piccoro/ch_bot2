from errbot import BotPlugin, botcmd, PY3
import json
from errbot.holder import bot
from config import CHATROOM_FN
from lxml import etree
from xml.etree.ElementTree import ParseError
import logging
from sleekxmpp.xmlstream.matcher import MatchXPath
#from sleekxmpp.xmlstream.handler import Callback
from sleekxmpp.xmlstream.handler import *

import sleekxmpp
from sleekxmpp import plugins, roster
from sleekxmpp.exceptions import IqError, IqTimeout

from sleekxmpp.stanza import Message, Presence, Iq, Error, StreamError
from sleekxmpp.stanza.roster import Roster
from sleekxmpp.stanza.nick import Nick
from sleekxmpp.stanza.htmlim import HTMLIM

from sleekxmpp.xmlstream import XMLStream, JID, tostring
from sleekxmpp.xmlstream import ET, register_stanza_plugin
from sleekxmpp.xmlstream.matcher import *
from sleekxmpp.xmlstream.handler import *
from errbot.backends.base import Message as Message_err_tmp

def get_sender_username(mess):
    """Extract the sender's user name from a message"""
    type = mess.getType()
    jid = mess.getFrom()
    if type == "groupchat":
        username = jid.getResource()
    elif type == "chat":
        username = jid.getNode()
    else:
        username = ""
    return username

class Message_err(Message_err_tmp):
    labdata = None
    labdata2 = None
    #zmiana = "ja zmienilem to"

class silf_plugin_base(BotPlugin):

    user_cwicz = ["",]
    adres_chat = ""
    adres_chat2 = ""
    adres_exp = ""
    exp_nick = ""
    zatrzymaj = False
    moj_pomiar = None
    adres_muc_exp = ""
    wiadomosc = ["Witaj, {}! Aby ze mną porozmawiać wpisz !czesio komenda w oknie czatu grupowego (jesli oczywiscie masz uprawnienia do pisania w nim)", "Lista komend dostepna jest po wpisaniu !czesio lista komend"]
    got_entire_roster = set()
    default_ns0 = "silf:results"
    default_ns1 = "silf:mode:get"
    default_ns2 = "silf:mode:set"
    default_ns3 = "silf:series:start"
    default_ns4 = "silf:experiment:stop"
    default_nsj = 'jabber:client'

    def __init__(self, pomiar ,adres_chatu = "", adres_exp = ""):
        self.moj_pomiar=pomiar
        self.adres_chat = adres_chatu
        self.adres_chat2 = adres_chatu[:adres_chatu.find("@")]
        self.adres_exp = adres_exp
        self.adres_muc_exp = adres_exp[:adres_exp.find("/")]
        self.exp_nick = adres_exp[adres_exp.find("/")+1:]
        super(silf_plugin_base, self).__init__()
        if self.adres_chat != "" and self.adres_exp != "":
            self.wejdz_do_pokoju()
        else:
            print("Blad. Brak mozliwosci wejscia do pokoju")
        bot.conn.add_event_handler('muc::{}::presence'.format(self.adres_muc_exp), self.kto_jest)
        bot.conn.add_event_handler('muc::%s::got_online' % self.adres_chat, self.callback_muc_presence)
        bot.conn.add_event_handler("message", self.incoming_message0)
        bot.conn.del_event_handler("message", bot.incoming_message)
        #bot.conn.client.removeHandler('IM_silf0')
        #bot.conn.client.removeHandler('IM_silf1')
        #bot.conn.client.removeHandler('IM_silf2')
        #bot.conn.client.removeHandler('IM_silf3')
        #bot.conn.client.removeHandler('IM_silf4')
        #bot.conn.client.registerHandler(Callback('IM_with_silf', MatchXPath('{%s}message' % (self.default_nsj)), self._bare_mess_silf0))
        #bot.conn.client.registerHandler(Callback('IM_silf0', MatchXPath('{%s}message/{%s}labdata' % (self.default_nsj, self.default_ns0)), self._bare_mess_silf))
        #bot.conn.client.registerHandler(Callback('IM_silf1', MatchXPath('{%s}message/{%s}labdata' % (self.default_nsj, self.default_ns1)), self._bare_mess_silf))
        #bot.conn.client.registerHandler(Callback('IM_silf2', MatchXPath('{%s}message/{%s}labdata' % (self.default_nsj, self.default_ns2)), self._bare_mess_silf))
        #bot.conn.client.registerHandler(Callback('IM_silf3', MatchXPath('{%s}message/{%s}labdata' % (self.default_nsj, self.default_ns3)), self._bare_mess_silf))
        #bot.conn.client.registerHandler(Callback('IM_silf4', MatchXPath('{%s}message/{%s}labdata' % (self.default_nsj, self.default_ns4)), self._bare_mess_silf))

    def incoming_message0(self, xmppmsg0):
        m = MatchXPath('{%s}message/{%s}labdata' % (self.default_nsj, self.default_ns0))
        t = m.match(xmppmsg0)
        print("\ntu sie zaczyna wazna sprawa\n{}\nlabdata\n{}\ntu sie konczy".format(xmppmsg0, t))
        msg = Message_err(xmppmsg0['body'])
        if 'html' in xmppmsg0.keys():
            msg.setHTML(xmppmsg0['html'])
        if t:
            xml0 = etree.fromstring(str(xmppmsg0))
            #etree.tostring(xml, pretty_print = True)
            #indent(xml)
            #etree.dump(xml)
            msg.labdata2 = xml0[1].text
            try:
                w1 = xml0.xpath('//t:labdata', namespaces={'t': self.default_ns0})
                if len(w1) > 0:
                    msg.labdata = w1[0].text
            except:
                msg.labdata = None
                msg.labdata2 = None
        msg.setFrom(xmppmsg0['from'].full)
        msg.setTo(xmppmsg0['to'].full)
        msg.setType(xmppmsg0['type'])
        msg.setMuckNick(xmppmsg0['mucnick'])
        msg.setDelayed(bool(xmppmsg0['delay']._get_attr('stamp')))  # this is a bug in sleekxmpp it should be ['from']
        bot.callback_message(bot.conn, msg)

    def _bare_mess_silf(self, xmppmsg):
        print("Wazne dobrze filtruje ?")

    def _bare_mess_silf0(self, xmppmsg):
        print("Wazne dobrze filtruje 0?")
        """Callback for message events"""
        print(xmppmsg)
        msg = Message_err(xmppmsg['body'])
        if 'html' in xmppmsg.keys():
            msg.setHTML(xmppmsg['html'])
        msg.setFrom(xmppmsg['from'].full)
        msg.setTo(xmppmsg['to'].full)
        msg.setType(xmppmsg['type'])
        #msg.setMuckNick(xmppmsg['mucnick'])
        #msg.setDelayed(bool(xmppmsg['delay']._get_attr('stamp')))  # this is a bug in sleekxmpp it should be ['from']
        #self.callback_message(self.conn, msg)

    def wejdz_do_pokoju(self):
        self.join_room(self.adres_chat, CHATROOM_FN)
        self.join_room(self.adres_muc_exp, CHATROOM_FN)

    def activate(self):
        super(silf_plugin_base, self).activate()

    def deactivate(self):
        bot.conn.del_event_handler('muc::{}::presence'.format(self.adres_muc_exp), self.kto_jest)
        bot.conn.del_event_handler('muc::%s::got_online' % self.adres_chat, self.callback_muc_presence)
        self.active = False
        super(silf_plugin_base, self).deactivate()

    def callback_muc_presence(self, pres):
        #print("zyjemy 00")
        if bot.mode == 'xmpp':
            room = pres['from'].node
            nick = pres['from'].resource
            #print("zyjemy 01")
            logging.debug("Presence for nick {} room {}.{}".format(nick, room, self.got_entire_roster))
            if room in self.got_entire_roster:
                msg = self.wiadomosc[0].format(pres['from'].resource)
                to = '{}/{}'.format(self.adres_chat, pres['from'].resource)
                #print("zyjemy 02")
                self.send(to, msg, message_type='chat')
                msg = self.wiadomosc[1]
                self.send(to, msg, message_type='chat')
            else:
                print("zyjemy 03")
                if nick == CHATROOM_FN:
                    logging.debug("Got entire roster in initial MUC presence "
                                 " broadcast for room {}.".format(self.adres_chat2))
                    self.got_entire_roster.add(self.adres_chat2)
                    return
                else:
                    logging.debug("Ignoring initial presence.")
                    return

    def pobierz_liste_uprawnionych(self):
        if self.ilosc_poller <= 0:
            self.stop_poller()
        mess="<presence to=\"{}\">" \
             "<priority>50<//priority>" \
             "<c xmlns=\"http://jabber.org/protocol/caps\" node=\"http://psi-dev.googlecode.com/caps\" ver=\"0.15\" ext=\"cs e-time ep-notify-2 html last-act mr sxe whiteboard\"/>" \
             "<x xmlns=\"http://jabber.org/protocol/muc\"/>" \
             "</presence>".format(self.adres_muc_exp + "/" + CHATROOM_FN)
        self.bare_send(mess)
        self.ilosc_poller -= 1

    def kto_jest(self, pres):
        xml = etree.fromstring(str(pres))
        print(xml.tag)
        try:
            if xml.get("type") == "unavailable":
                try:
                    self.user_cwicz.remove(xml.get("from"))
                    print("usunieto: {}".format(xml.get("from")))
                except ParseError as ee:
                    print("brak uzyt ({}) na liscie. Blad [{}]".format(xml.get("from")), str(ee))
                    pass
            else:
                try:
                    ns = "{http://jabber.org/protocol/muc#user}"
                    sxml = xml.findall('{0}x/{0}item'.format(ns))
                    if not sxml:
                        #print("brak x w present")
                        logging.debug("brak x")
                        return None
                    else:
                        for x in sxml:
                            if x.get("role") != "":
                                if x.get("role") == "moderator" or x.get("role") == "participant":
                                    if self.user_cwicz.count(x.get("nick")) == 0:
                                        self.user_cwicz.append(x.get("nick"))
                                else:
                                    try:
                                        self.user_cwicz.remove(x.get("nick"))
                                    except ParseError as ee:
                                        #print("brak uzyt ({}) na liscie. Blad [{}]".format(x.get("nick"), str(ee)))
                                        logging.debug("brak uzyt ({}) na liscie. Blad [{}]".format(x.get("nick"), str(ee)))
                except Exception as e:
                    #print("blad szczegolny : {}@@@{}".format(e, str(e)))
                    logging.debug("blad szczegolny : {}@@@{}".format(e, str(e)))
        except:
            #print("blad ogolny...")
            logging.debug("Error. []")
            pass

    def czykoniec(self, mess):
        m0 = str(mess)
        if m0.find("silf:series:stop") >= 0:
            return True
        return False

    def czygetok(self, mess):
        m0 = str(mess)
        if m0.find("silf:mode:get") >= 0:
            return True
        return False

    def czysetok(self, mess):
        m0 = str(mess)
        if m0.find("silf:mode:set") >= 0:
            return True
        return False

    def czystartok(self, mess):
        m0 = str(mess)
        if m0.find("silf:series:start") >= 0:
            m1 = m0.replace("silf:series:start", "")
            data = json.loads(m1)
            seriesid = data["seriesId"]
            self.moj_pomiar.series_id=seriesid
            return True
        return False

    def startexp(self, mess):
        if self.moj_pomiar.etap_startu == 1:
            if self.czygetok(mess):
                mess0 = self.moj_pomiar.mess_gen("SET", self.adres_muc_exp)
                print(mess0)
                self.bare_send(mess0)
                self.moj_pomiar.etap_startu = 2
        elif self.moj_pomiar.etap_startu == 2:
            if self.czysetok(mess):
                mess0 = self.moj_pomiar.mess_gen("START", self.adres_muc_exp)
                self.bare_send(mess0)
                self.moj_pomiar.etap_startu = 3
        elif self.moj_pomiar.etap_startu == 3:
            if self.czystartok(mess):
                self.moj_pomiar.etap_startu = 10
        return None

    def stopexp(self):
        #print("Zatrzymywanie experymentu")
        if self.moj_pomiar.etap_startu >= 10:
            mess0 = self.moj_pomiar.mess_gen("STOP", self.adres_muc_exp)
            self.bare_send(mess0)
            self.moj_pomiar.etap_startu = 0
            self.zatrzymaj = False

    def callback_contact_online(self, conn, mess):
        #print("[callback_contact_online] wartosc polaczenia: {}\n\nWartosc wiadomosci: {}".format(conn,mess))
        pass

    def callback_user_joined_chat(self, conn, mess):
        #print("[callback_user_joined_chat] wartosc polaczenia: {}\n\nWartosc wiadomosci: {}".format(conn,mess))
        pass

    def callback_message(self, conn, mess):
        if get_sender_username(mess) == "experiment":
            if self.moj_pomiar.etap_startu >= 0:
                if self.moj_pomiar.etap_startu < 10:
                    self.startexp(mess)
                elif self.czykoniec(mess):
                    if self.moj_pomiar.dane_ok == []:
                        self.moj_pomiar.zapisz_dane()
                    else:
                        self.moj_pomiar.test(True)
                    self.etap = 0
                    self.send(self.adres_chat, "Koniec serii lub przerwana seria", message_type="groupchat")
                else:
                    try:
                        print("\n\nLabdata\n{}\n".format(mess.labdata))
                        print("\n\nLabdata2\n{}\n".format(mess.labdata2))
                    except:
                        print("Brak labdaty")
                        #pass
                    mess0 = self.moj_pomiar.opisexp(mess)
                    #print("\n\nLabdata\n{}\n".format(mess.getLabdata()))
                    if mess0 != None:
                        self.send(self.adres_chat, mess0, message_type="groupchat")
            else:
                # log blad
                print("buuu jakis blad")
            if self.zatrzymaj == True:
                self.stopexp()

    def dobry_user(self, us):
        us = str(us)
        print("user {} i user_zm {}".format(us, us[us.find("/")+1:]))
        if us in self.user_cwicz:
            return True
        elif (us[us.find("/")+1:]) in self.user_cwicz:
            return True
        return False

    def czy_dobry_czat(self, mess):
        if mess.getFrom()[:mess.getFrom().find("/")] == self.adres_chat:
            return True
        return False
#'''
#
# zamienic nick na jid
#zmienic na admina
#'''

    @botcmd(admin_only=False, split_args_with=None)
    def qq(self, mess , user_name):
        print(self.moj_pomiar.dane_ok)
        return self.moj_pomiar.dane_ok

    @botcmd(admin_only=True, split_args_with=None)
    def pppp(self, mess , user_name):
        print("wartosc pppp:{}".format(self.pp))
        print("wartosc conn:{}".format(bot.conn))
        print("wartosc client:{}".format(bot.conn.client))
        print("wartosc roster:{}".format(bot.conn.client.roster))
        t = str(bot.conn.client.roster)
        t = t.replace("'", '"')
        t = t.replace("True", "true")
        t = t.replace("False", "false")
        print("t: {}".format(t))
        uzyts = json.loads(t)
        for uzyt in uzyts:
            print("uzyt:{}".format(bot.conn.client.roster[uzyt]))
            for uz in uzyt:
#                print("uz: {} || resouce:{}".format(uz, uz.resources))
                print("uz: {}".format(uz))

    @botcmd(admin_only=False, split_args_with="#")
    def rr(self, mess , arg):
        # reset danych referencyjnych (kolejny pomiar bedzie zbieral dane referencyjne)
        if self.dobry_user(mess.getFrom()):
            if self.moj_pomiar.dane_ok != []:
                self.moj_pomiar.dane_ok = []
                return "Zresetowano referencyjne dane pomiarowe. Dane z kolejnego pomiaru zostana zapisane jako referencyjne"
            return "Brak danych referencyjnych do resetowania wykonaj pomiar aby zapisac dane referencyjne"
        return "Brak uprawnien do wykonana komendy"

    @botcmd(admin_only=False, split_args_with="#")
    def sg_blad(self, mess , arg):
        # zmiana/odczyt tolerancji bledu pomiaru wzgledem referencji
        if self.dobry_user(mess.getFrom()):
            if len(arg) == 0:
                return "Aktualna tolerancja wzgledem referencyjnych danych pomiarowych wynosi {}.".format(self.moj_pomiar.rozrzut)
            elif len(arg) == 1:
                try:
                    tmp = float(arg[0])
                    if tmp > 1:
                        return "realy ?? blad wiekszy od wartosci ??"
                    if tmp < 0:
                        return "Nie ma ujemnych bledow (o ile mi wiadomo:)"
                    self.moj_pomiar.rozrzut = tmp
                    return "Blad ustawiono na {}".format(self.moj_pomiar.rozrzut)
                except ValueError:
                    return "Bledna wartosc parametru podanego jako wartosc toleracji"
            return "Bledna ilosc daych"
        return "Brak uprawnien do wykonana komendy"

    @botcmd(admin_only=False, split_args_with="#")
    def series_start(self, mess , arg):
        #print("wiadomosc {1} tresc {0}".format(mess, mess.getFrom()))
        if self.dobry_user(mess.getFrom()):
            mess0 = self.moj_pomiar.mess_gen("GET", self.adres_muc_exp)
            print(mess0)
            self.bare_send(mess0)
            self.moj_pomiar.etap_startu = 1

    @botcmd(admin_only=False, split_args_with="#")
    def series_stop(self, mess , arg):
        #print("wiadomosc {1} tresc {0}".format(mess, mess.getFrom()))
        if self.dobry_user(mess.getFrom()):
            print("dobry user")
        if True:
            self.zatrzymaj =True
            self.stopexp()

    @botcmd(split_args_with=None)
    def koniec_cwiczenia(self, mess, args):
        if self.dobry_user(mess.getFrom()):
            self.koniec_cwiczenia()

    @botcmd(split_args_with=None)
    def lista_komend(self, mess, args):
        return "Lista komend ogolnych dostepnych dla kazdego cwiczenia[pomoc], [lista komend],[info],[serie],[series start],[series stop],[koniec cwiczenia]"

    @botcmd(split_args_with=None)
    def pomoc(self, mess, args):
        return "pomoc - nadpisac funkcje w klasie dziedziczacej"
        #return "Możesz ze mną przeprowadzić doświadczenie Charakterystyka Licznika Geigera Mullera. Jeśli chcesz poznać ogólne informacje o doświadczeniu wpisz ``\czesio info``, jeśli chcesz zobaczyć co mogę Ci pokazać w tym doświadczeniu wpisz ''!czesio serie''"

    @botcmd(split_args_with=None)
    def info(self, mess, args):
        return "info o cwiczeniu - nadpisac funkcje w klasie dziedziczacej"
        #return "Tu jest informacja na temat exsperymentu.\nLink do ćwoczenia znajduje się tutaj: http://wp.pl"

    @botcmd(admin_only=False, split_args_with="#")
    def serie(self, mess, args):
        if self.dobry_user(mess.getFrom()):
            to = ""
            typ = mess.getType()
            if typ == "chat":
                to = get_sender_username(mess)
            else:
                to = self.adres_chat
                typ = "groupchat"
            for linia_opisu in self.moj_pomiar.opis_serie:
                self.send(to, linia_opisu, message_type = typ)
            return None

    @botcmd(split_args_with=None)
    def seria(self, mess, args):
        if self.dobry_user(mess.getFrom()):
            if args[0] == "ogólna":
                self.moj_pomiar = self.g_pomiar()
                self.send(self.adres_chat, self.moj_pomiar.mess_gen("GET", self.adres_exp), message_type="groupchat")
                return "Rozpoczynam ćwiczenie. Ustawienia domyślne {} Startujemy serię pomiarową! Seria ta dokona ogólnego skanu i pokaże jak zależy ilość zliczeń od napięcia na lampie w całym zakresie napięć. Od tej chwili nie masz możliwości kondtolowania doświadczenia, ponieważ  kontroluję go ja. By odzyskać kontrolę napisz ``\czesio seria stop``.".format(self.moj_pomiar.opis())
            elif args[0] == "stop":
                self.stopexp()
                return "Przerywan ćwiczenie na żądanie użytkownika"
            else:
                return "Błędne lub nie kompletne dane wejściowe."
        else:
            return "Nie mozesz wykonywac cwiczenia. Pogadaj z adminem ;)"



