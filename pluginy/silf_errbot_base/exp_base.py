from uuid import uuid4


class pomiar():

    etap_startu = 0
    czas_pomiaru = 0
    liczba_punktow = 0
    etap = 0
    id = 0
    series_id = 0
    opis_serie = ["",]
    rozrzut = 0

    def __init__(self, czas_pomiaru=0, liczba_punktow=0):
        self.czas_pomiaru = czas_pomiaru
        self.liczba_punktow = liczba_punktow
        self.etap = 0

    def pomiar(self, czas_pomiaru=0, liczba_punktow=0):
        self.czas_pomiaru = czas_pomiaru
        self.liczba_punktow = liczba_punktow
        self.etap = 0

    def mozna_wykonac(self):
        if self.u_stop !=0:
            if self.u_stop != 0:
                if self.liczba_punktow != 0:
                    if self.czas_pomiaru != 0:
                        if self.etap == 0:
                            return True
        return False

    def opis(self):
        return "Prosze zmienic opis experymentu"

    def opisexp(self, mess):
        return "Prosze zmienic opis wartosci w trakcie dzialania experymentu"

    def mess_gen(self, typ, gdzie):
        id = uuid4().urn
        if typ == "GET":
            return "<message type=\"groupchat\" to=\"{}\"><labdata xmlns=\"silf:mode:get\" type=\"query\" id=\"{}\"/></message>".format(gdzie, id)
        elif typ == "SET":
            return  "<message type=\"groupchat\" to=\"{}\"><labdata xmlns=\"silf:mode:set\" type=\"query\" id=\"{}\">{{\"mode\":\"defaut\"}}</labdata></message>".format(gdzie, id)
        elif typ == "START":
            return "<message type=\"groupchat\" to=\"{}\"><labdata xmlns=\"silf:series:start\" type=\"query\" id=\"{}\">{{\"acquisition_time\":{{\"value\":10}},\"point_count\":{{\"value\":10}},\"start_voltage\":{{\"value\":300}},\"end_voltage\":{{\"value\":950}}}}</labdata></message>".format(gdzie, id)
        elif typ == "STOP":
            return "<message type=\"groupchat\" to=\"{}\"><labdata xmlns=\"silf:series:stop\" type=\"result\" id=\"{}\">{{\"seriesId\": \"{}\"}}</labdata></message>".format(gdzie, id, self.series_id)
        else:
            print("zly typ w funkcji mess_gen")
            return None


    def test(self, ostat = False):
        #zdefiniowac odpowiednia fukcje testujaca dla eksperymentu
        return None

    def zapisz_dane(self):
        #zdefiniowac zapis danych referencyjnych dla ekperymentu
        return None


# format_labdata do wyciagania
# LabdataModeGet itp. do wstawiania
# zmienic changes z mastera git

