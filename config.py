import logging
import os
CURR_DIR = os.path.split(os.path.abspath(__file__))[0]
#print(CURR_DIR)
BOT_DATA_DIR = os.path.join(CURR_DIR, 'err-log')
BOT_EXTRA_PLUGIN_DIR = os.path.join(CURR_DIR, 'pluginy')
BOT_LOG_FILE = os.path.join(CURR_DIR, 'err-log/err.log')
BOT_LOG_LEVEL = logging.DEBUG
BOT_LOG_SENTRY = False
SENTRY_DSN = ''
SENTRY_LOGLEVEL = BOT_LOG_LEVEL
BOT_ASYNC = True

BOT_IDENTITY = {
    'username': 'bot1@localhost/test',  # The JID of the user you have created for the bot
    'password': 'haslobot1',       # The corresponding password for this user
}
BOT_ADMINS = ('admin@debian','admin@localhost','Admin@VM','tester02',)
CHATROOM_PRESENCE = (('chat_bot_admin@muc.debian','trudne_haslo'),)
CHATROOM_FN = 'Bot_v_0.9'
BOT_PREFIX = '!czesio '

#BOT_PREFIX_OPTIONAL_ON_CHAT = False
#BOT_ALT_PREFIXES = ('Err',)
#BOT_ALT_PREFIX_SEPARATORS = (':', ',', ';')
#BOT_ALT_PREFIX_CASEINSENSITIVE = True
ACCESS_CONTROLS_DEFAULT = {} # Allow everyone access by default
#ACCESS_CONTROLS = {'status': {'allowrooms': ('someroom@conference.localhost',)},
#                   'about': {'denyusers': ('baduser@localhost',), 'allowrooms': ('room1@conference.localhost', 'room2@conference.localhost')},
#                   'uptime': {'allowusers': BOT_ADMINS},
#                   'help': {'allowmuc': False},
#                  }
#HIDE_RESTRICTED_COMMANDS = False
#HIDE_RESTRICTED_ACCESS = False
DIVERT_TO_PRIVATE = ('help', 'about', 'status')
# DIVERT_TO_PRIVATE = ()
CHATROOM_RELAY = {}
REVERSE_CHATROOM_RELAY = {}
#MESSAGE_SIZE_LIMIT = 10000
XMPP_CA_CERT_FILE = None
XMPP_FEATURE_MECHANISMS = {}
#XMPP_FEATURE_MECHANISMS =  {'use_mech': 'PLAIN', 'unencrypted_plain': True, 'encrypted_plain': False}
#XMPP_KEEPALIVE_INTERVAL = 300
#IRC_CHANNEL_RATE = 1
#IRC_PRIVATE_RATE = 1
